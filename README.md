# Creación de Aplicación desktop

# **Detalles**
esto es un proyecto con arquitectura en capas en donde tenemos las principales presentación, negocios, datos y la capa común o capa transversal.

Tambien se hace uso de los principios de usabilidad de la interacion humano computador creando asi una interfaz de usuario y experiencia de usuarios amigable.

# Pre-condiciones
en la carpeta base de datos existen dos scripts "Creacion de la base de datos" para crear las tablas y relaciones.
"Prueba de Store Procedure" para crear los procedimientos almacenados relacionados con la Aplicacion.

guardar el proyecto en la ruta C:\Users\user\source\repos de esta forma no tendra que generar configuraciones adicionales.

# configurar la conexión a ms SQL server.

En la capa de datos modificar contructor de la clase conectionSql, remplazar del atributo 
"ConecctionString" la referencia al servidor sustituyendola por el local que tienen en su equipo.

**ejemplo**
ConecctionString = "Server = Su servidor local; DataBase = PRUEBAS; integrated security = true";

# instalación de paquetes NuGet
necesitara font awesome.sharp para los iconos de los botones y circularProgressBar para la pantalla de bienvenida.

omitir estos paquetes causaran problemas en el codigo de diseño de los formularios lo que traera problemas con el funcionamiento.

