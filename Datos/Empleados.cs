﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaComun;
namespace Datos
{
    public class Empleados: ConeccionSql
    {
        private string User;
        private int ID;
        private string Nombre;
        private string apellido;
        private string cargo;
        private string correoElectronico;

        public Empleados ()
        {
            
        }
        //public Empleados (int id, string nombre, string apellido, string cargo, string correoElectronico)
        //{
        //    this.ID = id;
        //    this.Nombre = nombre;
        //    this.apellido = apellido;
        //    this.cargo = cargo;
        //    this.correoElectronico = correoElectronico;
        //}
        public bool Obtener (string a)
        {
            return ObtenerDatos(a);
        }    
        public bool Inisec(string a, string b)
        {

            return IniciarSeccion(a, b);
           
        }
        public bool VEmpleado (string a , string b)
        {
            return ValidarEmpleado(a, b);
        }
        public bool Vusuario (string a , string b)
        {
            return ValidarUsuario(a, b);
        }

        private bool IniciarSeccion (string user, string password)
        {
            using (var conecction = GetSqlConnection())
            {
                conecction.Open();
                using (var comand = new SqlCommand())
                {
                    comand.Connection = conecction;
                    comand.CommandText= "ValidacionUsuario";
                    comand.Parameters.AddWithValue( "@UserID", user);
                    comand.Parameters.AddWithValue( "@passoword", password);
                    comand.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = comand.ExecuteReader();

                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            this.User=  reader.GetString(0);
                        }
                        return true;

                        
                    }

                    else return false;
                }
            }
            
             
        }
       

        private bool ValidarEmpleado(string a, string b)
        {
            using (var conecction = GetSqlConnection())
            {
                conecction.Open();
                using (var comand = new SqlCommand())
                {
                    comand.Connection = conecction;
                    comand.CommandText = "ValidacionEmpleados";
                    comand.Parameters.AddWithValue("@UserID", a);
                    comand.Parameters.AddWithValue("@passoword", b);
                    comand.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = comand.ExecuteReader();

                    if (reader.HasRows)
                    {
                        return true;
                    }

                    else return false;
                }
            }


        }

        private bool ValidarUsuario(string a, string b)
        {
            using (var conecction = GetSqlConnection())
            {
                conecction.Open();
                using (var comand = new SqlCommand())
                {
                    comand.Connection = conecction;
                    comand.CommandText = "validacionhabilitado";
                    comand.Parameters.AddWithValue("@UserID", a);
                    comand.Parameters.AddWithValue("@passoword", b);
                    comand.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = comand.ExecuteReader();

                    if (reader.HasRows)
                    {
                        return true;
                    }

                    else return false;
                }
            }


        }
        private bool ObtenerDatos(String user)
        {
            using (var conecction = GetSqlConnection())
            {
                conecction.Open();
                using (var comand = new SqlCommand())
                {
                    comand.Connection = conecction;
                    comand.CommandText = "SP_obtenerDatos";
                    comand.Parameters.AddWithValue("@UserID", user);
                    comand.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = comand.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            CapaComun.Usuario.ID = reader.GetInt32(0);
                            CapaComun.Usuario.Nombre = reader.GetString(1);
                            CapaComun.Usuario.apellido = reader.GetString(2);
                            CapaComun.Usuario.cargo = reader.GetString(3);
                            CapaComun.Usuario.correoElectronico = reader.GetString(4);

                        }
                        return true;
                    }

                    else return false;
                }
            }
        }

        //public  List<Empleados> ObtenerDatos(string User)
        //{
        //    User = "aFerreras";
        //    using (var conecction = GetSqlConnection())
        //    {
        //        conecction.Open();
        //        using (var comand = new SqlCommand())
        //        {
        //            comand.Connection = conecction;
        //            comand.CommandText = "SP_obtenerDatos";
        //            comand.Parameters.AddWithValue("@UserID", User);
        //            comand.CommandType = CommandType.StoredProcedure;
        //            SqlDataReader reader = comand.ExecuteReader();

        //            List<Empleados> empleados = new List<Empleados>();

        //            while (reader.Read())
        //            {
        //                empleados.Add(new Empleados
        //                {
        //                    ID = reader.GetInt32(0),
        //                    Nombre = reader.GetString(1),
        //                    apellido = reader.GetString(2),
        //                    cargo = reader.GetString(3),
        //                    correoElectronico = reader.GetString(4),
        //                });


        //            }
        //            return empleados;
        //        }


        //    }

        //}


       
    }   
}
    

