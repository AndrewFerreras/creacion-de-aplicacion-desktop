﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaComun
{
    public class Usuario
    {
        //Static se utliza cuando no se crearan objetos con esa clase , para que todos los sitios que tienen de referencia puedan acceder al mismo

            //ejemplo si se hubira instanciado la clase usuario desde login y desde pantalla de inicio sin tener los campos static ubiera arrojado elementos diferentes
            //ya que no se trataba del mismo objeto.

            //otra anotacion es la auto propiedad get y set su uso mayormente es cuando no se necesitan validaciones o algo por el estilo
        public static string User{ get; set; }
        public static int ID { get; set; }
        public static string Nombre { get; set; }
        public static string apellido { get; set; }
        public static string cargo { get; set; }
        public static string correoElectronico { get; set; }


        
    }
}
