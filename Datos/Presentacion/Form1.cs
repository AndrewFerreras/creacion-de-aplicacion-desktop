﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Negocios;
namespace Presentacion
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            {//evento de mover formulario
                ReleaseCapture();
                SendMessage(this.Handle, 0x112, 0xf012, 0);

            }
        }
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            {//evento de mover  desde la imagen formulario
                ReleaseCapture();
                SendMessage(this.Handle, 0x112, 0xf012, 0);

            }
        }
       

        

   

        private void textUsuario_Enter(object sender, EventArgs e)
        {
            textUsuario.Text = "";

        }

        private void textUsuario_Leave(object sender, EventArgs e)
        {
            if (textUsuario.Text == "") textUsuario.Text = "Usuario:";
            
        }

        private void textcontra_Enter(object sender, EventArgs e)
        {
            if (textcontra.Text == "Contraseña:")
            {
                textcontra.Text = "";

                textcontra.UseSystemPasswordChar = true;
            }
        }

        private void textcontra_Leave(object sender, EventArgs e)
        {
            if (textcontra.Text == "")
            {
                textcontra.Text = "Contraseña:";
                textcontra.UseSystemPasswordChar = false;

            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bLogin_Click(object sender, EventArgs e)
        {
            Nempleados nempleados = new Nempleados();
            if (textUsuario.Text != "")
            {
                if (textcontra.Text != "")
                {
                    
                    var Validador = nempleados.DinicioSeccion(textUsuario.Text, textcontra.Text);
                    if (Validador ==true )
                    {
                        nempleados.obtener(textUsuario.Text);
                       
                       var ValidarUsuario= nempleados.dvalidarusuario(textUsuario.Text, textcontra.Text);
                        if (ValidarUsuario == true)
                        {
                            var ValidarEmp = nempleados.DvalidarEmpleado(textUsuario.Text, textcontra.Text);

                            if (ValidarEmp == true)
                            {
                                
                                this.Hide();
                                PantallaBienvenida bienvenida = new PantallaBienvenida();
                                bienvenida.ShowDialog();
                                InterfazCeo interfazCeo = new InterfazCeo();
                                interfazCeo.Show();
                                interfazCeo.FormClosed += cerrarseccion; //evento que muestra el formulario de login cuando se cierra el menu principal.
                            }
                            else
                            {
                               
                                Error.Visible = true;
                                MessageBox.Show ("Tu usuario y contraseña son correctos pero \n actualmente no figuras como un colaborador  activo es necesario que le dejes saber a tu jefe inmediato,","Advertencia", MessageBoxButtons.OK,MessageBoxIcon.Warning);
                                Error.Visible = false;
                            }




                        }
                        else
                        {
                            
                            message.Visible = false;
                            Error.Visible = true;
                            MessageBox.Show("Tu usuario y contraseña son correctos pero \n actualmente tu usuario no esta habilitado es necesario que le dejes saber a tu jefe inmediato", "Advertencia",MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            Error.Visible = false;
                        }




                    }
                    else
                    {
                        message.Visible = false;
                        Mensaje("Usuario o contraseña incorrectos por favor intente de nuevo");
                        textUsuario.Focus();
                        textcontra.Clear();

                    }
                }
                else
                {
                    Mensaje("Por favor introduzca su contraseña");
                    textcontra.Focus();
                }
            }
            else
            {
                Mensaje("Por favor introdusca su usuario");
                textUsuario.Focus();

            }

        }
        


        private void Mensaje(string contenido)
        {
            message.Visible = true;

            message.Text = contenido;

        }
        private void cerrarseccion(object sender, FormClosedEventArgs e)
        {
            textcontra.Text = "Contraseña:";
            textUsuario.Text = "Usuario:";
            message.Visible = false;
            textcontra.UseSystemPasswordChar = false;
            this.Show();

        }
        
    }
}
