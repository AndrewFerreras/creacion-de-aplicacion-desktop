﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;

namespace Presentacion
{
    public partial class InterfazCeo : Form
    {

        private IconButton Button;
        private Panel BordeDerecho;


        public InterfazCeo()
        {
            InitializeComponent();
            BordeDerecho = new Panel();
            BordeDerecho.Size = new Size(7, 39);

            BarraMenu.Controls.Add(BordeDerecho);



            //arraztrar

            this.Text = string.Empty;
            this.ControlBox = false;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;


        }

        private void ActivarBoton(Object boton) // paso como parametro un objeto de tipo object debido a que ese es el tipo de objecto que pasa el evento click
        {
            DesactivarBoton(); //metodo para desabilitar el efecto del boton anterior que fue pursado

            if (boton != null) //creo condicion por seguridad y buena practica de solo hacer el proceso en caso de que el parametro no sea null
            {
                Button = (IconButton)boton;
                Button.BackColor = Color.FromArgb(27, 32, 58);
                Button.TextAlign = ContentAlignment.MiddleCenter;
                Button.IconColor = Color.FromArgb(24, 237, 207);
                Button.TextImageRelation = TextImageRelation.TextBeforeImage;
                Button.ImageAlign = ContentAlignment.MiddleRight;

                BordeDerecho.BackColor = Color.FromArgb(24, 237, 207);
                BordeDerecho.Location = new Point(0, Button.Location.Y);
                BordeDerecho.Visible = true;
                BordeDerecho.BringToFront();
            }
        }
        private void DesactivarBoton()
        {
            if (Button != null)
            {
                Button.BackColor = Color.FromArgb(37, 44, 72);
                Button.TextAlign = ContentAlignment.MiddleRight;
                Button.IconColor = Color.FromArgb(255, 255, 255);
                Button.TextImageRelation = TextImageRelation.ImageBeforeText;
                Button.ImageAlign = ContentAlignment.MiddleLeft;


            }
        }
        private void Productos_Click(object sender, EventArgs e)
        {
            ActivarBoton(sender);
        }

        private void Inventario_Click(object sender, EventArgs e)
        {
            ActivarBoton(sender);
        }
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void paneBarraHorizontal_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }



        private void paneBarraHorizontal_DoubleClick(object sender, EventArgs e)
        {

        }

        private void iconClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void iconMaximizar_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = FormBorderStyle.None;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.FormBorderStyle = FormBorderStyle.Sizable;



            }

        }

        private void InterfazCeo_Load(object sender, EventArgs e)
        {

        }
    }
}