﻿namespace Presentacion
{
    partial class Login
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.textUsuario = new System.Windows.Forms.TextBox();
            this.textcontra = new System.Windows.Forms.TextBox();
            this.bLogin = new System.Windows.Forms.Button();
            this.linkContra = new System.Windows.Forms.LinkLabel();
            this.message = new System.Windows.Forms.Label();
            this.Error = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // textUsuario
            // 
            this.textUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textUsuario.Font = new System.Drawing.Font("Arcon Rounded-", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textUsuario.ForeColor = System.Drawing.Color.White;
            this.textUsuario.Location = new System.Drawing.Point(389, 134);
            this.textUsuario.Name = "textUsuario";
            this.textUsuario.Size = new System.Drawing.Size(355, 20);
            this.textUsuario.TabIndex = 2;
            this.textUsuario.Text = "Usuario:";
            this.textUsuario.Enter += new System.EventHandler(this.textUsuario_Enter);
            this.textUsuario.Leave += new System.EventHandler(this.textUsuario_Leave);
            // 
            // textcontra
            // 
            this.textcontra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textcontra.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textcontra.Font = new System.Drawing.Font("Arcon Rounded-", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textcontra.ForeColor = System.Drawing.SystemColors.Window;
            this.textcontra.Location = new System.Drawing.Point(389, 180);
            this.textcontra.Name = "textcontra";
            this.textcontra.Size = new System.Drawing.Size(355, 20);
            this.textcontra.TabIndex = 3;
            this.textcontra.Text = "Contraseña:";
            this.textcontra.Enter += new System.EventHandler(this.textcontra_Enter);
            this.textcontra.Leave += new System.EventHandler(this.textcontra_Leave);
            // 
            // bLogin
            // 
            this.bLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(237)))), ((int)(((byte)(207)))));
            this.bLogin.FlatAppearance.BorderSize = 2;
            this.bLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(237)))), ((int)(((byte)(207)))));
            this.bLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLogin.Font = new System.Drawing.Font("Arcon Rounded-", 12F);
            this.bLogin.ForeColor = System.Drawing.SystemColors.Window;
            this.bLogin.Location = new System.Drawing.Point(488, 234);
            this.bLogin.Name = "bLogin";
            this.bLogin.Size = new System.Drawing.Size(186, 35);
            this.bLogin.TabIndex = 1;
            this.bLogin.Text = "INGRESAR";
            this.bLogin.UseVisualStyleBackColor = true;
            this.bLogin.Click += new System.EventHandler(this.bLogin_Click);
            // 
            // linkContra
            // 
            this.linkContra.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.linkContra.AutoSize = true;
            this.linkContra.Font = new System.Drawing.Font("Arcon Rounded-", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkContra.LinkColor = System.Drawing.Color.White;
            this.linkContra.Location = new System.Drawing.Point(501, 278);
            this.linkContra.Name = "linkContra";
            this.linkContra.Size = new System.Drawing.Size(157, 14);
            this.linkContra.TabIndex = 5;
            this.linkContra.TabStop = true;
            this.linkContra.Text = "¿Haz olvidado tu contaseña?";
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Font = new System.Drawing.Font("Arcon Rounded-", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.message.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(237)))), ((int)(((byte)(207)))));
            this.message.Location = new System.Drawing.Point(47, 278);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(38, 14);
            this.message.TabIndex = 7;
            this.message.Text = "label1";
            this.message.Visible = false;
            // 
            // Error
            // 
            this.Error.Image = global::Presentacion.Properties.Resources.Animacion;
            this.Error.Location = new System.Drawing.Point(12, 23);
            this.Error.Name = "Error";
            this.Error.Size = new System.Drawing.Size(360, 282);
            this.Error.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.Error.TabIndex = 8;
            this.Error.TabStop = false;
            this.Error.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Presentacion.Properties.Resources._25210161;
            this.pictureBox1.Location = new System.Drawing.Point(12, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(360, 282);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.ClientSize = new System.Drawing.Size(791, 332);
            this.Controls.Add(this.Error);
            this.Controls.Add(this.message);
            this.Controls.Add(this.linkContra);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.bLogin);
            this.Controls.Add(this.textcontra);
            this.Controls.Add(this.textUsuario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Login_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.Error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox textUsuario;
        private System.Windows.Forms.TextBox textcontra;
        private System.Windows.Forms.Button bLogin;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.LinkLabel linkContra;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.PictureBox Error;
    }
}

