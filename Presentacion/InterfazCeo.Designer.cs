﻿namespace Presentacion
{
    partial class InterfazCeo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BarraMenu = new System.Windows.Forms.Panel();
            this.Usuarios = new FontAwesome.Sharp.IconButton();
            this.Productos = new FontAwesome.Sharp.IconButton();
            this.iconMaximizar = new FontAwesome.Sharp.IconPictureBox();
            this.iconClose = new FontAwesome.Sharp.IconPictureBox();
            this.Contenedor = new System.Windows.Forms.Panel();
            this.NombreUsuario = new System.Windows.Forms.Label();
            this.BarraMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconMaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconClose)).BeginInit();
            this.SuspendLayout();
            // 
            // BarraMenu
            // 
            this.BarraMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(44)))), ((int)(((byte)(72)))));
            this.BarraMenu.Controls.Add(this.Usuarios);
            this.BarraMenu.Controls.Add(this.Productos);
            this.BarraMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.BarraMenu.Location = new System.Drawing.Point(0, 0);
            this.BarraMenu.Name = "BarraMenu";
            this.BarraMenu.Size = new System.Drawing.Size(200, 613);
            this.BarraMenu.TabIndex = 0;
            this.BarraMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.BarraMenu_Paint);
            // 
            // Usuarios
            // 
            this.Usuarios.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(44)))), ((int)(((byte)(72)))));
            this.Usuarios.FlatAppearance.BorderSize = 0;
            this.Usuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Usuarios.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.Usuarios.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Usuarios.ForeColor = System.Drawing.Color.White;
            this.Usuarios.IconChar = FontAwesome.Sharp.IconChar.User;
            this.Usuarios.IconColor = System.Drawing.Color.White;
            this.Usuarios.IconSize = 30;
            this.Usuarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Usuarios.Location = new System.Drawing.Point(3, 84);
            this.Usuarios.Name = "Usuarios";
            this.Usuarios.Rotation = 0D;
            this.Usuarios.Size = new System.Drawing.Size(197, 38);
            this.Usuarios.TabIndex = 1;
            this.Usuarios.Text = "Usuarios";
            this.Usuarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Usuarios.UseVisualStyleBackColor = true;
            this.Usuarios.Click += new System.EventHandler(this.Usuarios_Click);
            // 
            // Productos
            // 
            this.Productos.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(44)))), ((int)(((byte)(72)))));
            this.Productos.FlatAppearance.BorderSize = 0;
            this.Productos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Productos.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.Productos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Productos.ForeColor = System.Drawing.Color.White;
            this.Productos.IconChar = FontAwesome.Sharp.IconChar.Tag;
            this.Productos.IconColor = System.Drawing.Color.White;
            this.Productos.IconSize = 30;
            this.Productos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Productos.Location = new System.Drawing.Point(3, 160);
            this.Productos.Name = "Productos";
            this.Productos.Rotation = 0D;
            this.Productos.Size = new System.Drawing.Size(197, 38);
            this.Productos.TabIndex = 0;
            this.Productos.Text = "Productos";
            this.Productos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Productos.UseVisualStyleBackColor = true;
            this.Productos.Click += new System.EventHandler(this.Productos_Click);
            // 
            // iconMaximizar
            // 
            this.iconMaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconMaximizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.iconMaximizar.IconChar = FontAwesome.Sharp.IconChar.WindowMaximize;
            this.iconMaximizar.IconColor = System.Drawing.Color.White;
            this.iconMaximizar.IconSize = 23;
            this.iconMaximizar.Location = new System.Drawing.Point(1229, 3);
            this.iconMaximizar.Name = "iconMaximizar";
            this.iconMaximizar.Size = new System.Drawing.Size(23, 25);
            this.iconMaximizar.TabIndex = 2;
            this.iconMaximizar.TabStop = false;
            this.iconMaximizar.Click += new System.EventHandler(this.iconMaximizar_Click);
            // 
            // iconClose
            // 
            this.iconClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.iconClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.iconClose.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.iconClose.IconColor = System.Drawing.Color.White;
            this.iconClose.IconSize = 23;
            this.iconClose.Location = new System.Drawing.Point(1261, 3);
            this.iconClose.Name = "iconClose";
            this.iconClose.Size = new System.Drawing.Size(23, 25);
            this.iconClose.TabIndex = 3;
            this.iconClose.TabStop = false;
            this.iconClose.Click += new System.EventHandler(this.iconClose_Click);
            // 
            // Contenedor
            // 
            this.Contenedor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Contenedor.Location = new System.Drawing.Point(200, 45);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(1096, 568);
            this.Contenedor.TabIndex = 4;
            this.Contenedor.Visible = false;
            // 
            // NombreUsuario
            // 
            this.NombreUsuario.AutoSize = true;
            this.NombreUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NombreUsuario.ForeColor = System.Drawing.Color.White;
            this.NombreUsuario.Location = new System.Drawing.Point(221, 13);
            this.NombreUsuario.Name = "NombreUsuario";
            this.NombreUsuario.Size = new System.Drawing.Size(46, 18);
            this.NombreUsuario.TabIndex = 5;
            this.NombreUsuario.Text = "label1";
            // 
            // InterfazCeo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.ClientSize = new System.Drawing.Size(1296, 613);
            this.Controls.Add(this.NombreUsuario);
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.iconClose);
            this.Controls.Add(this.iconMaximizar);
            this.Controls.Add(this.BarraMenu);
            this.Name = "InterfazCeo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "InterfazCeo";
            this.Load += new System.EventHandler(this.InterfazCeo_Load);
            this.BarraMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.iconMaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconClose)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel BarraMenu;
        private FontAwesome.Sharp.IconButton Productos;
        private FontAwesome.Sharp.IconPictureBox iconMaximizar;
        private FontAwesome.Sharp.IconPictureBox iconClose;
        private FontAwesome.Sharp.IconButton Usuarios;
        private System.Windows.Forms.Panel Contenedor;
        private System.Windows.Forms.Label NombreUsuario;
    }
}