﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class UsuariosF : Form
    {
        public UsuariosF()
        {
            InitializeComponent();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void textNombre_Enter(object sender, EventArgs e)
        {
            if (textNombre.Text == "Nombre:") textNombre.Text = "";
        }

        private void textNombre_Leave(object sender, EventArgs e)
        {
            if (textNombre.Text == "") textNombre.Text = "Nombre:";
        }
        private void textApellidos_Enter(object sender, EventArgs e)
        {
            if (textApellidos.Text == "Apellidos:") textApellidos.Text = "";
        }

        private void textApellidos_Leave(object sender, EventArgs e)
        {
            if (textApellidos.Text == "") textApellidos.Text = "Apellidos:";
        }

        private void textCorreo_Enter(object sender, EventArgs e)
        {
            if (textCorreo.Text == "Correo Electronico:") textCorreo.Text = "";
        }

        private void textCorreo_Leave(object sender, EventArgs e)
        {
            if (textCorreo.Text == "") textCorreo.Text = "Correo Electronico:";
        }

        private void textDireccion_Enter(object sender, EventArgs e)
        {
            if (textDireccion.Text == "Direccion:") textDireccion.Text = "";
        }

        private void textDireccion_Leave(object sender, EventArgs e)
        {
            if (textDireccion.Text == "") textDireccion.Text = "Direccion:";
        }

        private void textCedula_Enter(object sender, EventArgs e)
        {
            if (textCedula.Text == "Cedula:") textCedula.Text = "";
        }

        private void textCedula_Leave(object sender, EventArgs e)
        {
            if (textCedula.Text == "") textCedula.Text = "Cedula:";
        }
      

        private void textCelular_Enter(object sender, EventArgs e)
        {
            if (textCelular.Text == "Numero de celular:") textCelular.Text = "";
        }

        private void textCelular_Leave(object sender, EventArgs e)
        {
            if (textCelular.Text == "") textCelular.Text = "Numero de celular:";
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Cargo_Enter(object sender, EventArgs e)
        {
            if (Cargo.Text == "Cargo") Cargo.Text = "";
        }

        private void Cargo_Leave(object sender, EventArgs e)
        {

            if (Cargo.Text == "") Cargo.Text = "Cargo";
        }
    }
}
