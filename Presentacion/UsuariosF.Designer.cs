﻿namespace Presentacion
{
    partial class UsuariosF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Cargo = new System.Windows.Forms.ComboBox();
            this.textCelular = new System.Windows.Forms.TextBox();
            this.textCedula = new System.Windows.Forms.TextBox();
            this.textDireccion = new System.Windows.Forms.TextBox();
            this.textCorreo = new System.Windows.Forms.TextBox();
            this.textApellidos = new System.Windows.Forms.TextBox();
            this.textNombre = new System.Windows.Forms.TextBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.textUsuario = new System.Windows.Forms.TextBox();
            this.textClave = new System.Windows.Forms.TextBox();
            this.bLogin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Cargo
            // 
            this.Cargo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Cargo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.Cargo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cargo.ForeColor = System.Drawing.Color.White;
            this.Cargo.FormattingEnabled = true;
            this.Cargo.Location = new System.Drawing.Point(50, 379);
            this.Cargo.Name = "Cargo";
            this.Cargo.Size = new System.Drawing.Size(131, 28);
            this.Cargo.TabIndex = 19;
            this.Cargo.Text = "Cargo";
            this.Cargo.Enter += new System.EventHandler(this.Cargo_Enter);
            this.Cargo.Leave += new System.EventHandler(this.Cargo_Leave);
            // 
            // textCelular
            // 
            this.textCelular.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textCelular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textCelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCelular.ForeColor = System.Drawing.Color.White;
            this.textCelular.Location = new System.Drawing.Point(50, 327);
            this.textCelular.Name = "textCelular";
            this.textCelular.Size = new System.Drawing.Size(268, 19);
            this.textCelular.TabIndex = 18;
            this.textCelular.Text = "Numero de celular:";
            this.textCelular.Enter += new System.EventHandler(this.textCelular_Enter);
            this.textCelular.Leave += new System.EventHandler(this.textCelular_Leave);
            // 
            // textCedula
            // 
            this.textCedula.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textCedula.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textCedula.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textCedula.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCedula.ForeColor = System.Drawing.Color.White;
            this.textCedula.Location = new System.Drawing.Point(50, 288);
            this.textCedula.Name = "textCedula";
            this.textCedula.Size = new System.Drawing.Size(355, 19);
            this.textCedula.TabIndex = 17;
            this.textCedula.Text = "Cedula:";
            this.textCedula.Enter += new System.EventHandler(this.textCedula_Enter);
            this.textCedula.Leave += new System.EventHandler(this.textCedula_Leave);
            // 
            // textDireccion
            // 
            this.textDireccion.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textDireccion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textDireccion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textDireccion.ForeColor = System.Drawing.Color.White;
            this.textDireccion.Location = new System.Drawing.Point(50, 244);
            this.textDireccion.Name = "textDireccion";
            this.textDireccion.Size = new System.Drawing.Size(355, 19);
            this.textDireccion.TabIndex = 16;
            this.textDireccion.Text = "Direccion:";
            this.textDireccion.Enter += new System.EventHandler(this.textDireccion_Enter);
            this.textDireccion.Leave += new System.EventHandler(this.textDireccion_Leave);
            // 
            // textCorreo
            // 
            this.textCorreo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textCorreo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textCorreo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textCorreo.ForeColor = System.Drawing.Color.White;
            this.textCorreo.Location = new System.Drawing.Point(50, 205);
            this.textCorreo.Name = "textCorreo";
            this.textCorreo.Size = new System.Drawing.Size(355, 19);
            this.textCorreo.TabIndex = 15;
            this.textCorreo.Text = "Correo Electronico:";
            this.textCorreo.Enter += new System.EventHandler(this.textCorreo_Enter);
            this.textCorreo.Leave += new System.EventHandler(this.textCorreo_Leave);
            // 
            // textApellidos
            // 
            this.textApellidos.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textApellidos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textApellidos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textApellidos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textApellidos.ForeColor = System.Drawing.Color.White;
            this.textApellidos.Location = new System.Drawing.Point(50, 162);
            this.textApellidos.Name = "textApellidos";
            this.textApellidos.Size = new System.Drawing.Size(355, 19);
            this.textApellidos.TabIndex = 14;
            this.textApellidos.Text = "Apellidos:";
            this.textApellidos.Enter += new System.EventHandler(this.textApellidos_Enter);
            this.textApellidos.Leave += new System.EventHandler(this.textApellidos_Leave);
            // 
            // textNombre
            // 
            this.textNombre.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textNombre.ForeColor = System.Drawing.Color.White;
            this.textNombre.Location = new System.Drawing.Point(50, 116);
            this.textNombre.Name = "textNombre";
            this.textNombre.Size = new System.Drawing.Size(355, 19);
            this.textNombre.TabIndex = 13;
            this.textNombre.Text = "Nombre:";
            this.textNombre.Enter += new System.EventHandler(this.textNombre_Enter);
            this.textNombre.Leave += new System.EventHandler(this.textNombre_Leave);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(221, 450);
            this.dateTimePicker1.MinDate = new System.DateTime(1901, 1, 1, 0, 0, 0, 0);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(97, 20);
            this.dateTimePicker1.TabIndex = 20;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(46, 450);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 20);
            this.label1.TabIndex = 21;
            this.label1.Text = "Fecha de nacimiento";
            // 
            // textUsuario
            // 
            this.textUsuario.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textUsuario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textUsuario.ForeColor = System.Drawing.Color.White;
            this.textUsuario.Location = new System.Drawing.Point(495, 116);
            this.textUsuario.Name = "textUsuario";
            this.textUsuario.Size = new System.Drawing.Size(355, 19);
            this.textUsuario.TabIndex = 22;
            this.textUsuario.Text = "Usuario:";
            // 
            // textClave
            // 
            this.textClave.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textClave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.textClave.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textClave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textClave.ForeColor = System.Drawing.Color.White;
            this.textClave.Location = new System.Drawing.Point(495, 174);
            this.textClave.Name = "textClave";
            this.textClave.Size = new System.Drawing.Size(355, 19);
            this.textClave.TabIndex = 23;
            this.textClave.Text = "Contraseña:";
            // 
            // bLogin
            // 
            this.bLogin.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.bLogin.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(237)))), ((int)(((byte)(207)))));
            this.bLogin.FlatAppearance.BorderSize = 2;
            this.bLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(237)))), ((int)(((byte)(207)))));
            this.bLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.bLogin.ForeColor = System.Drawing.SystemColors.Window;
            this.bLogin.Location = new System.Drawing.Point(495, 435);
            this.bLogin.Name = "bLogin";
            this.bLogin.Size = new System.Drawing.Size(186, 35);
            this.bLogin.TabIndex = 24;
            this.bLogin.Text = "REGISTRAR";
            this.bLogin.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(348, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 29);
            this.label2.TabIndex = 25;
            this.label2.Text = "Crear Usuario";
            // 
            // UsuariosF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(32)))), ((int)(((byte)(58)))));
            this.ClientSize = new System.Drawing.Size(859, 505);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bLogin);
            this.Controls.Add(this.textClave);
            this.Controls.Add(this.textUsuario);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.Cargo);
            this.Controls.Add(this.textCelular);
            this.Controls.Add(this.textCedula);
            this.Controls.Add(this.textDireccion);
            this.Controls.Add(this.textCorreo);
            this.Controls.Add(this.textApellidos);
            this.Controls.Add(this.textNombre);
            this.Name = "UsuariosF";
            this.Text = "UsuariosF";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox Cargo;
        private System.Windows.Forms.TextBox textCelular;
        private System.Windows.Forms.TextBox textCedula;
        private System.Windows.Forms.TextBox textDireccion;
        private System.Windows.Forms.TextBox textCorreo;
        private System.Windows.Forms.TextBox textApellidos;
        private System.Windows.Forms.TextBox textNombre;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textUsuario;
        private System.Windows.Forms.TextBox textClave;
        private System.Windows.Forms.Button bLogin;
        private System.Windows.Forms.Label label2;
    }
}